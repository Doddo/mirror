
/*============================
=            Init            =
============================*/

var app = angular.module('myApp', ['ngRoute']);


/*================================================
=            Directives personalisées            =
================================================*/



app.directive("ngBlur", function(){
	return function(scope, elem, attr){
		elem.bind("blur", function(){
			scope.$apply(attr.ngBlur)
		})
	}
})

/*===================================
=            Application            =
===================================*/

app.controller("TodoController", function($scope, filterFilter, $http, $location){

	/*=================================================
	=            Declaration des variables            =
	=================================================*/

	$scope.todos = [];
	$scope.placeholer = "Chargement...";
	$scope.statusFilter = "";
	$scope.editing = false;
	/*=================================
	=            Functions            =
	=================================*/

	/*----------  Récupérer todos  ----------*/
	
	
	var interval = setInterval(function(){
		getTodo()
	}, 1000)

	$scope.stopInterval = function () {
		clearInterval(interval)
	}

	/*----------  Supprimer une todo  ----------*/
	
 	$scope.removeTodo = function (index, todo){
 		$scope.todos.splice(index, 1);

		$http.put(
				"../scripts/deleteTodo.php",
				{
					id: todo.id
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)
		}, function(resp){
			// console.log(resp)
		});
 	}

 	/*----------  Créer une todo  ----------*/
 	
 	$scope.addTodo = function(){

 		// Ajoute le todo
		$http.post("../scripts/createTodo.php",{name: $scope.newtodo},{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)
		}, function(resp){
			// console.log(resp)
		});

		$http.get("scripts/getTodo.php").success(function(data){
			$scope.todos = data
			$scope.placeholder = "Nouvelle tache...";
			console.log(data)
		}).error(function(){
			console.log("erreur chargement des todo")
		})

		$scope.newtodo = "";
	}
	
	/*----------  Update Todo  ----------*/

 	$scope.editTodo = function (todo) {
 		

 		if (todo.name == "") {
 			todo.editing = false;
 			return;
 		}

		$http.put(
				"../scripts/updateTodo.php",
				{
					name: todo.name, 
					id: todo.id
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		}).then(function(data){
			// console.log(data)

			interval = setInterval(function(){
				getTodo()
			}, 1000)
			
		}, function(resp){
			// console.log(resp)
		});

 		todo.editing = false;

 	}

 	/*----------  Check Todo  ----------*/
 	
 	$scope.checkTodo = function(todo) {
 		if (todo.completed == 0){
			todo.completed = 1
 		} else if (todo.completed == 1){
			todo.completed = 0
 		}


		$http.put(
				"../scripts/checkTodo.php",
				{
					name: todo.name, 
					id: todo.id,
					completed: todo.completed
				},
				{'Content-Type': 'application/x-www-form-urlencoded'
		})
 	}

 	/*----------  Filtre footer  ----------*/
 
 	if ($location.path() == "") { $location.path("/")}
 	$scope.location = $location;
	$scope.$watch("location.path()", function(path){

		if (path == "/active") {
			$scope.statusFilter = {completed : "0"}
		} else if (path == "/done"){
			$scope.statusFilter = {completed : "1"}
		} else {
			$scope.statusFilter = ""
		}

	})
	
	/*----------  Récupère la liste des todos  ----------*/

	function getTodo () {
		if ($scope.editing == true){
			clearInterval(interval)
		}
		$http.get("../scripts/getTodo.php").success(function(data){
			$scope.todos = data
			$scope.placeholder = "Nouvelle tache...";
			// console.log(data)
		}).error(function(){
			// console.log("erreur chargement des todo")
		})
	}

})



