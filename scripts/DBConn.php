<?php
	
	class DBConn{

		function __construct(){
			include("../config/DBConn.php");
           	$dsn = "mysql:dbname=tfe;host=127.0.0.1;charset=utf8";
            $user = "root";

			try {
	            $this->dbh = new \PDO($dsn, $user, $passWord);
	            $this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

	        } catch (PDOException $e) {
	            echo "DBConn error";
	        }
		}
	}
