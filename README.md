# Interactive Mirror #
Sources for the Interactive Mirror.
Built in HTML - CSS - JS and PHP

## Material list ##
* Raspberry Pi with Raspbian
* Innolux 7inches touchscreen (or equivalent)
* Ethernet or Wifi dongle
* One-way film


## Getting started ##
To install the Interactive Mirror, set-up Raspbian on your Raspberry Pi (maybe compatible with other linux distros).

Login to SSH and follow these instructions

### Update everything ###

```
#!bash

sudo apt-get update
sudo apt-get upgrade
```

### Install Apache and PHP5 ###
```
#!bash

sudo apt-get install apache2 php5 libapache2-mod-php5 -y
```

### Install MySQL ###
```
#!bash

sudo apt-get install mysql-server mysql-client php5-mysql
```
### Install git ###

```
#!bash

sudo apt-get install git
```


### Clone the repository in /var/www/interactive ###

```
#!bash

git clone https://Doddo@bitbucket.org/tfe/mirror.git
```

## Configuration ##
Now, the Interactive Mirror should be installed. Let's configure it.

Add a virtual Host

```
#!bash

sudo nano /etc/apache2/sites-available/interactive.conf
```
Paste this:

```
#!bash

<VirtualHost *:80>
        DocumentRoot /var/www/interactive
        ErrorLog /var/www/interactive/error.log
        CustomLog /var/www/interactive/access.log combined
</VirtualHost>
```
Disable the default virtual host and enable the new one:

```
#!bash

sudo a2dissite 000-default.conf
sudo a2ensite interactive.conf
```
### Run the mirror at startup ###
To run the mirror at startup, follow this tutorial

https://gist.github.com/jmsaavedra/7dd81746596cdda1544f

Be carefull. When you edit the file, replace 

@chromium-browser --noerrordialogs --disable-session-crashed-bubble --disable-infobars --kiosk http://raspberry-locale-ip/desktop.html

"raspberry-locale-ip" should look like '192.168.x.x" (not 127.0.0.1 or 192.168.x.1 or 192.168.x.0)

## Test ##
To test the Interactive Mirror, open your web browser and access the IP you used to SSH (should look like 192.168.x.x).

## Look at your mirror! ##
Now, connected the Raspberry Pi to your screen and appose a "way film" on the screen.

## This project is down for now ##

Enjoy!